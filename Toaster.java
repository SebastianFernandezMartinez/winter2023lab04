public class Toaster {

    private int temp;
    private int time;
    private String cookingType;
    private String color;
    
    public void printWork(){
        System.out.println("The toaster is "+cookingType+" at "+temp+" degrees "+" during "+time+" minutes.");
    }
    public void printBrand(){
        System.out.println("brand toaster");
    }
    public Toaster(int temp,int time,String cookingType, String color){
        this.temp = temp;
        this.time = time;
        this.cookingType = cookingType;
        this.color = color;
    }
    public void setTemp(int temp){
        this.temp = temp;
    }
    public void setTime(int time){
        this.time = time;
    }
    public void setColor(String color){
        this.color = color;
    }
    public void setCookingType(String cookingType){
        this.cookingType = cookingType;
    }
    public int getTemp(){
        return temp;
    }
    public int getTime(){
        return this.time;
    }
    public String getCookingType(){
        return this.cookingType;
    }
    public String getColor(){
        return this.color;
    }
    
    private boolean breadState(){
        if (time > 15){
            this.color = "black";
        }
        if (color == "black"){
            return true;
        }
        return false;
        }
    
    public void checkIfBreadBurnt(){
        if (breadState()){
            System.out.println("it is burnt");
        }else {
            System.out.println("it is not burnt");
        }
    }
    }
